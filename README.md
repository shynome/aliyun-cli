## cdn

### RefreshObjectCaches - 刷新cdn 

```yml
pipeline:
  flash_cdn:
    image: shynome/aliyun-cli
    secrets: [ access_key_id, access_key_secret ]
    command: 
    - aliyun cdn RefreshObjectCaches https://baidu.com
```

触发 cdn 刷新的方法:

- 提交信息包含: `[flush cdn]`
- 提交信息包含: `[cdn refresh]`
- 打标签事件