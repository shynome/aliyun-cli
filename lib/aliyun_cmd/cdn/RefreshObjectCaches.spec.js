const assert = require('assert')
const shell = require('shelljs')
const { _exports:{ tip } } = require('./RefreshObjectCaches')

describe('RefreshObjectCaches',()=>{

  let origin = 'https://baidu.com'
  
  beforeEach(()=>{
    // clear 
    delete process.env['DRONE_COMMIT_MESSAGE']
    delete process.env['DRONE_BUILD_EVENT']
  })
  
  it('no include commit flag str',()=>{

    // smailer string
    for(let msg of [ 'cdn', 'flash', 'flash cdn', ] ){

      process.env['DRONE_COMMIT_MESSAGE'] = msg
      process.env['DRONE_BUILD_EVENT'] = 'push'

      const { stdout } = /**@type {shell.ExecOutputReturnValue}*/(shell.exec(`aliyun cdn RefreshObjectCaches ${origin}`,{ silent: true }))

      assert.deepStrictEqual(
        stdout.replace(/\n$/,''),
        tip,
        JSON.stringify([msg,tip,stdout])
      )

    }
    
  })
  
  it('origin',()=>{

    let commit_word = [ '[flash cdn]', '[refresh cdn]', '[cdn refresh]', '[cdn flush]', '[flush cdn]' ]
    // RegExp match string
    for(let msg of [ ...commit_word, ...commit_word.map(a=>a.toUpperCase()), 'tag' ] ){

      process.env['DRONE_COMMIT_MESSAGE'] = msg
      process.env['DRONE_BUILD_EVENT'] = msg==='tag'? 'tag' : 'push'
      const { code, stderr, stdout } = /**@type {shell.ExecOutputReturnValue}*/(shell.exec(`aliyun cdn RefreshObjectCaches ${origin}`,{ silent: true }))
      
      let exceptRes = "请求更新缓存失败"
      
      assert.deepStrictEqual(
        stdout.slice(0,exceptRes.length),
        exceptRes,
        JSON.stringify([msg,stdout])
      )

    }

  })
  
})