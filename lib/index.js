exports.util = require('@shynome/drone-utils')

exports.init = ()=>
require('yargs')
.alias('h','help')
.alias('v','version')
.demandCommand(1)
.help()

