FROM node:carbon-alpine

ENV NODE_ENV=production

COPY npm-pack.tgz /tmp/npm-pack.tgz
RUN set -x \
  && npm i -g /tmp/npm-pack.tgz \
  && rm -rf /tmp/npm-pack.tgz $(npm config get cache)

WORKDIR /app
CMD [ "aliyun", "-h" ]
