FROM node:carbon-alpine

ENV NODE_ENV=production

ARG PACKAGE_TAG=latest

RUN npm i -g @shynome/aliyun-cli@${PACKAGE_TAG} \
  && rm -rf /tmp/npm-pack.tgz $(npm config get cache)

WORKDIR /app
CMD [ "aliyun", "-h" ]
