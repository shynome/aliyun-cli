# CHANGELOG

## 1.0.5

### ADD

- 多添加了几个触发关键词, 现在触发条件为:
  - 提交信息包含以下关键词(不区分大小写): `[flash cdn]`, `[refresh cdn]`, `[cdn refresh]`, `[cdn flush]`, `[flush cdn]`
  - 提交 tag